# Projet Modelica Usine Marémotrice

Auteurs : Paul Sourisse, Matthieu Lucas, Blanche Grimm, Patrick Lelu

## Description du projet 
Notre projet a pour but de modéliser le fonctionnement de l'usine marémotrice de la Rance en Ille-et-Vilaine grâce au logiciel Modelica.


## Contenu du répertoire

Le code est organisé dans le package `UsineProject` qui contient :

* Une simulation `UsineTest`
* Les composants principaux de la simulation : `Usine`, `Mer` et `LaRance`
* Un sous package `Composants` contenant les composants de l'usine : `Turbines`, `Vannes` et la `Commande` (qui permet de faire le lien entre tout ce joli monde)
* Un sous package `Interfaces` contenant les connecteurs : `WaterPort`, un connecteur basé sur l'eau et `SignalPort`, un connecteur pemettant de communiquer avec les différents éléments.

Le projet est accessible dans le répertoire https://gitlab-student.centralesupelec.fr/blanche.grimm/projet-modelica-usine-maremotrice

# Pour réaliser la simulation

- Dans le fichier "UsineProject", ouvrir "package.mo" pour ouvrir le code complet
- Dans OMEdit, ouvrir "UsineTest"
- Lancer la simulation
- Afficher LaRance/S_Rance/s et mer/S_mer/s pour voir le niveau d'eau de La Rance et de la mer
- Afficher Turbines/P pour observer la puissance produite
- Observer command/S_turbines/s et command/S_vannes/s pour observer les changements d'état des composants

