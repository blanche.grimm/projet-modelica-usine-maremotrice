within UsineProject;

model LaRance
  constant Height Href = 1 "Reference height of la Rance";
  Area S "Area of la Rance";
  Area S0 = 4000000 "Area of la Rance without water";
  Height pente_bassin = 1.37 * 1000000 "Pente du bassin";
  VolumeFlowRate q_eau;
  Volume V;
  Interfaces.WaterPort turbines annotation(
    Placement(transformation(origin = {-100, 18}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 18}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort vannes annotation(
    Placement(transformation(origin = {-100, -40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, -40}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.SignalPort S_Rance annotation(
    Placement(transformation(origin = {0, -98}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}})));
equation
  S_Rance.s = turbines.H;
  S_Rance.s = vannes.H;
  S = pente_bassin*turbines.H + S0;
  q_eau = turbines.q + vannes.q;
  q_eau = der(V);
  q_eau = S*der(turbines.H);
annotation(
    Diagram(graphics = {Polygon(origin = {0, -36}, fillColor = {138, 175, 50}, fillPattern = FillPattern.Solid, points = {{-100, 56}, {-94, 60}, {-86, 62}, {-76, 64}, {-70, 64}, {-64, 62}, {-58, 58}, {-50, 54}, {-44, 52}, {-34, 50}, {-26, 48}, {-18, 48}, {-10, 50}, {-4, 52}, {0, 54}, {4, 56}, {12, 60}, {18, 62}, {24, 62}, {34, 60}, {40, 58}, {46, 56}, {50, 54}, {54, 52}, {58, 50}, {66, 48}, {72, 48}, {76, 50}, {82, 54}, {88, 58}, {92, 60}, {96, 62}, {100, 62}, {100, -64}, {-100, -64}, {-100, -64}, {-100, 56}})}),
  Icon(graphics = {Rectangle(fillColor = {73, 164, 249}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Polygon(origin = {0, -36}, fillColor = {86, 169, 107}, fillPattern = FillPattern.Solid, points = {{-100, 56}, {-94, 62}, {-86, 64}, {-78, 64}, {-68, 62}, {-62, 60}, {-58, 58}, {-52, 56}, {-44, 52}, {-34, 50}, {-26, 48}, {-18, 48}, {-10, 50}, {-4, 52}, {2, 56}, {6, 58}, {12, 60}, {18, 62}, {26, 62}, {34, 60}, {40, 58}, {46, 56}, {50, 54}, {54, 52}, {58, 50}, {64, 48}, {70, 48}, {76, 50}, {82, 54}, {88, 58}, {92, 60}, {96, 62}, {100, 64}, {100, -64}, {-100, -64}, {-100, -64}, {-100, 56}})}));
end LaRance;