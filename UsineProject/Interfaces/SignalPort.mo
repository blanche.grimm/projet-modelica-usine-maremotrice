within UsineProject.Interfaces;

connector SignalPort
  Real s "Command signal";
  annotation(
    Icon(graphics = {Rectangle(fillColor = {192, 28, 40}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}})}));
end SignalPort;