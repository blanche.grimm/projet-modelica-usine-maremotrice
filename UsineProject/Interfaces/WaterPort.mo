within UsineProject.Interfaces;

connector WaterPort
  Height H "Height at the connection" ;
  flow VolumeFlowRate q(displayUnit = "m3/h") "Volume flow rate into the connection";annotation(
    Icon(graphics = {Ellipse(fillColor = {96, 164, 253}, fillPattern = FillPattern.Solid, extent = {{-80, 80}, {80, -80}})}));
  
end WaterPort;
