within UsineProject.Composants;

model Commande
  Interfaces.SignalPort S_Rance annotation(
    Placement(transformation(origin = {-100, 42}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 40}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.SignalPort S_mer annotation(
    Placement(transformation(origin = {-100, -40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, -40}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.SignalPort S_turbines annotation(
    Placement(transformation(origin = {100, 40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 40}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.SignalPort S_vannes annotation(
    Placement(transformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}})));
equation
if integer(S_turbines.s)==0 then
    if S_Rance.s-S_mer.s>=5 then
      S_turbines.s=2;
      S_vannes.s=0;
    elseif S_Rance.s-S_mer.s<=0.1 then
      S_turbines.s=1;
      S_vannes.s=1;
    else
      S_turbines.s=0;
      S_vannes.s=0;
    end if;
  elseif integer(S_turbines.s)==1 then
    if S_Rance.s>=10 then
      S_turbines.s=3;
      S_vannes.s=0;
    else
      S_turbines.s=1;
      S_vannes.s=1;
    end if;
  elseif integer(S_turbines.s)==2 then
    if S_Rance.s-S_mer.s<=0.5 then
      S_turbines.s=0;
      S_vannes.s=0;
    else
      S_turbines.s=2;
      S_vannes.s=0;
    end if;
  else
    if S_Rance.s-S_mer.s>=3 then
      S_turbines.s=0;
      S_vannes.s=0;
    else
      S_turbines.s=3;
      S_vannes.s=0;
    end if;
  end if;
      

annotation(
    Diagram);


end Commande;