within UsineProject.Composants;

model Turbines
  
  Height dH "difference of height";
  VolumeFlowRate q_eau "volume flow rate";
  Modelica.Units.SI.Power P "Power";
  
  constant Modelica.Units.SI.Density rho=998.2 "Density of salt water";
  constant Modelica.Units.SI.Acceleration g = Modelica.Constants.g_n;
  parameter Real alpha_mR = 1820;
  parameter Real alpha_Rm = -1420;
  parameter Real qConstCouplage=3000;
  parameter Real qConstPompage=-3000;
  
  Interfaces.WaterPort LaRance annotation(
    Placement(transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.SignalPort S_turbines annotation(
    Placement(transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort mer annotation(
    Placement(transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}})));

equation
  dH = LaRance.H - mer.H;
  
  if integer(S_turbines.s) == 0 then
  //turbines fermées
    q_eau = 0;
    P = 0;
  elseif integer(S_turbines.s) == 1 then
  //turbines à vide
    if dH > 0 then
      q_eau = 24 * alpha_mR * sqrt(abs(dH));
    else
      q_eau = 24 * alpha_Rm * sqrt(abs(dH));
    end if;
    
    P = 0;
  elseif integer(S_turbines.s) == 2 then
  //turbines couplées
    if dH > 0 then
      q_eau = qConstCouplage;
    else
      q_eau = -qConstCouplage;
    end if;
    P = rho * g * dH * abs(q_eau);
  else
  //pompage
    q_eau = qConstPompage;
    P = rho * g * dH * q_eau;
  end if;
  LaRance.q = mer.q;
  q_eau = LaRance.q;
  
  annotation(
    Icon(graphics = {Ellipse(extent = {{-25, 25}, {25, -25}}), Polygon(origin = {3, -42}, rotation = 140, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-11, -40}, rotation = 120, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-25, -34}, rotation = 100, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-35, -22}, rotation = 80, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-41, -10}, rotation = 60, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-43, 6}, rotation = 40, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-37, 20}, rotation = 20, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-29, 30}, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-15, 36}, rotation = 340, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-1, 42}, rotation = 320, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {15, 40}, rotation = 300, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {27, 32}, rotation = 280, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {39, 22}, rotation = 260, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {45, 8}, rotation = 240, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {43, -6}, rotation = 220, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {37, -20}, rotation = 200, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {29, -34}, rotation = 180, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {17, -44}, rotation = 160, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Ellipse(fillColor = {112, 112, 112}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{-35, 35}, {35, -35}})}));


end Turbines;