within UsineProject.Composants;

model Vannes

  Height dH "difference of height";
 
  parameter Real beta_mR = 1.51079e-2 "From the sea to la Rance";
  parameter Real beta_Rm = 3.37798e-2 "From la Rance to the sea";
  Interfaces.SignalPort S_vannes annotation(
    Placement(transformation(origin = {2, -100}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {2, -100}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort mer annotation(
    Placement(transformation(origin = {-102, -2}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {-102, -2}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort LaRance annotation(
    Placement(transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}})));
    
equation
  LaRance.q = mer.q;
  dH = LaRance.H - mer.H;
  
  if integer(S_vannes.s) == 1 then
    if dH > 0 then
      mer.q = 6 * 4 * dH / beta_mR;
    else
      mer.q = 6 * 4 * dH / beta_Rm;
      
    end if;
    
  else
    mer.q = 0;
  end if;
  annotation(
    Icon(graphics = {Rectangle(fillColor = {73, 164, 249}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Rectangle(origin = {0, -30}, fillColor = {109, 109, 109}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-100, 70}, {100, -70}}), Rectangle(origin = {-44, -54}, lineColor = {0, 0, 255}, fillColor = {0, 85, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-16, 46}, {16, -46}}), Rectangle(origin = {44, -54}, lineColor = {0, 0, 255}, fillColor = {0, 85, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-16, 46}, {16, -46}}), Rectangle(origin = {-44, 0}, lineColor = {0, 0, 255}, fillColor = {77, 77, 77}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-16, 20}, {16, -20}}), Rectangle(origin = {44, 0}, lineColor = {0, 0, 255}, fillColor = {77, 77, 77}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-16, 20}, {16, -20}})}));


end Vannes;