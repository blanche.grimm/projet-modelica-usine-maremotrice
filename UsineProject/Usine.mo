within UsineProject;

model Usine
  Mer mer annotation(
    Placement(transformation(origin = {-70, 10}, extent = {{-10, -10}, {10, 10}})));
  LaRance laRance annotation(
    Placement(transformation(origin = {70, 10}, extent = {{-10, -10}, {10, 10}})));
  Composants.Commande commande annotation(
    Placement(transformation(origin = {2, -50}, extent = {{-10, -10}, {10, 10}})));  
  Composants.Turbines turbines annotation(
    Placement(transformation(origin = {0, 32}, extent = {{-10, -10}, {10, 10}})));
  Composants.Vannes vannes annotation(
    Placement(transformation(origin = {0, -10}, extent = {{-10, -10}, {10, 10}})));
equation
  connect(mer.turbines, turbines.mer) annotation(
    Line(points = {{-60, 12}, {-50, 12}, {-50, 32}, {-10, 32}}));
  connect(turbines.LaRance, laRance.turbines) annotation(
    Line(points = {{10, 32}, {50, 32}, {50, 12}, {60, 12}}));
  connect(mer.vannes, vannes.mer) annotation(
    Line(points = {{-60, 6}, {-50, 6}, {-50, -10}, {-10, -10}}));
  connect(vannes.LaRance, laRance.vannes) annotation(
    Line(points = {{10, -10}, {50, -10}, {50, 6}, {60, 6}}));
  connect(mer.S_mer, commande.S_mer) annotation(
    Line(points = {{-70, 0}, {-70, -54}, {-8, -54}}));
  connect(commande.S_Rance, laRance.S_Rance) annotation(
    Line(points = {{-8, -46}, {-20, -46}, {-20, -32}, {70, -32}, {70, 0}}));
  connect(commande.S_turbines, turbines.S_turbines) annotation(
    Line(points = {{12, -46}, {20, -46}, {20, 10}, {0, 10}, {0, 22}}));
  connect(vannes.S_vannes, commande.S_vannes) annotation(
    Line(points = {{0, -20}, {0, -26}, {40, -26}, {40, -54}, {12, -54}}));

  annotation (
    Icon);
end Usine;