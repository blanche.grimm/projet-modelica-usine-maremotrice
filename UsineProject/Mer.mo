within UsineProject;
model Mer
  constant Height H0 = (12-4.5) "Max height";
  constant Real pi = Modelica.Constants.pi;
  Modelica.Units.SI.Frequency f = 27 / (14 * 86400) "Frequency of tide";
  
  Interfaces.SignalPort S_mer annotation(
    Placement(transformation(origin = {0, -98}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, -98}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort vannes annotation(
    Placement(transformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}})));
  Interfaces.WaterPort turbines annotation(
    Placement(transformation(origin = {100, 20}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {100, 20}, extent = {{-10, -10}, {10, 10}})));
equation
  vannes.H = turbines.H;
  turbines.H = H0/2 * (1 + sin(2*pi*f*time)) + 4.5;
  S_mer.s = turbines.H;
annotation(
    Icon(graphics = {Rectangle(fillColor = {73, 164, 249}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Polygon(origin = {0, -23}, fillColor = {26, 95, 180}, fillPattern = FillPattern.Solid, points = {{-100, -77}, {-100, 43}, {-92, 61}, {-78, 71}, {-64, 77}, {-38, 77}, {-22, 71}, {-8, 61}, {6, 51}, {20, 45}, {40, 41}, {52, 41}, {60, 41}, {74, 45}, {84, 49}, {100, 63}, {100, -77}, {-100, -77}})}));
end Mer;